import { Link} from "react-router-dom"

const CarList = (props) => {
  console.log("props carlist: ", props)
  const {car} = props

  console.log("ini car listtt")
  return (
    <div className="car-list">
        {/* <h3>Ini carlisttt</h3> */}
         <div class="row card-group">
            {car.map((car)=> ( 
              <div class="col-6 col-sm-4 col-md-4 col-lg-3 kartu">
                <img class="card-img-top" src={car.image} alt="Card image cap"/>
                <div class="card-body">
                  <h5 class="card-title">{car.name}</h5>
                  <h4>Rp {car.price}</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <Link to={`/result/${car.id}`}>
                   <button>Pilih Mobil</button>
                </Link>
              </div>
            ))}
          </div>
    </div>
  );
}

export default CarList;
