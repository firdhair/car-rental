import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"

import useAxios from '../hooks/useAxios'
import Nav from '../components/nav/Nav'
import Footer from '../components/footer/Footer'

const CarDetail = () => {
    const { response } = useAxios('https://rent-cars-api.herokuapp.com/admin/car');   
    const [detail, setDetail] = useState([])
    let storage = []
    
    const {id} = useParams()

    useEffect(()=> {
        if(response.length !== undefined){
            for(let i = 0; i < response.length; i++) {
                if(response[i]['id'] == id){
                    storage = response[i]
                    console.log("storage: ", storage)
                    setDetail(storage)
                }
            }
        }
    }, [response])  
    
    return (
        <>
        <Nav/>
        <div className="row car-detail">
            {/* <h3>Ini car detaiiiiiiiiiiiiiiil</h3> */}
            <div className="col-6 col-sm-4 col-md-4 col-lg-7 car-detail__info-rental info">
                <div className="info-rental-box">
                    <h5>Tentang Paket</h5>
                <p>Include</p>
                <ul>
                    <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                    <li>Sudah termasuk bensin selama 12 jam</li>
                    <li>Sudah termasuk tiket wisata</li>
                    <li>Sudah termasuk pajak</li>
                </ul>
                <p>Exclude</p>
                <ul>
                    <li>Tidak termasuk biaya makan sopir Rp. 75.000/hari</li>
                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp.20.000/jam</li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                </ul>
                <h5 className="refund">Refund, Reschedule, Overtime</h5>
                <ul>
                    <li>Tidak termasuk biaya makan sopir Rp. 75.000/hari</li>
                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp.20.000/jam</li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                    <li>Tidak termasuk biaya makan sopir Rp. 75.000/hari</li>
                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp.20.000/jam</li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                </ul>
                </div>
                <button>Lanjutkan Pembayaran</button>
            </div>
            <div className="col-6 col-sm-4 col-md-4 col-lg-4 car-detail__info-mobil info">
                 <img class="card-img-top" src={detail['image']} alt="Card image cap"/>
                 <div>
                     <h5 className="info-mobil-name">{detail['name']}</h5>
                 </div>
                 <div className="info-mobil__pembayaran">
                     <div className="pembayaran-harga">
                         <p>Total</p>
                         <h5>Rp. {detail['price']}</h5>
                     </div>
                     <button>Lanjutkan Pembayaran</button>
                 </div>
            </div>
        </div>
        <Footer/>
        </>
    );
}

export default CarDetail;
