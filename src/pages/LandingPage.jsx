import React from 'react'

import Nav from '../components/nav/Nav';
import Hero from '../components/hero/Hero';
import Search from '../components/search/Search';
import Footer from '../components/footer/Footer';

const LandingPage = () => {
    return (
        <>
        <div className="main">
            <Nav/>
            <Hero/>
        </div>
        <Search/>
        <Footer/>
        </>
    )
}

export default LandingPage