import {Routes, Route} from "react-router-dom"

import LandingPage from './pages/LandingPage'
import CarDetail from './pages/CarDetail'
import Login from './pages/Login'

import './styles/style.css';


const App = () => {
  return (
    <section>
      <Routes>
          <Route path="/" element={<LandingPage/>}/>    
          <Route path="/login" element={<Login/>}></Route>
          <Route path="/result/:id" element={<CarDetail/>}></Route>
      </Routes>
    </section>
  );
}

export default App;
