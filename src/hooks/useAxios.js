import {useState, useEffect} from "react";
import axios from "axios"

export default function useAxios(url){
    console.log("ini axios")
    const [loading, setLoading] = useState(false)
    const [response, setResponse] = useState({})
    const [error, setError] = useState({})

    useEffect(() => {
    setLoading(true);
    // async function getData() {
    //   try{
    //     const response = await axios.get(url);
    //     setResponse(response.data);
    //     setLoading(false);
    //   } catch(error){
    //     setError(error)
    //     setLoading(false);
    //   }
    // }

    // getData()
    axios.get(url)
      .then((response) => {
        setLoading(false);
        console.log("ini response data: ", response.data)
        setResponse(response.data);
      })
      .catch((error) => {
        setLoading(false);
        setError(error);
      });
  }, [url]);

    return {loading, response, error}
}