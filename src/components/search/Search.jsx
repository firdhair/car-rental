import React, { useState } from "react";
import {useNavigate } from "react-router-dom"

import CarList from "../../pages/CarList";
import useAxios from "../../hooks/useAxios";

const Search = () => {
  const [tipe, setTipe] = useState("")
  const [car, setCar] = useState([])
  let str2 = []
  let history = useNavigate()

  
  const { response } = useAxios('https://rent-cars-api.herokuapp.com/admin/car');    

  const handleSearch = (e) => {
    e.preventDefault()
    history('/')
      
    for(let i = 0; i < response.length; i++) {
      if(response[i]['name'] !== null && response[i]['image'] !== null) {
        console.log("response[i]['name']", response[i]['name'])   
        if(response[i]['name'].toLowerCase().search(tipe) >= 0){
          str2.push(response[i])
        }
      }
    }
    setCar(str2)
  }

  return (
   <>
    <form onSubmit={handleSearch}>
      <div className="search-container">
        <div className="search-container__tipe search-box">
            <p>Cari Mobil</p>
            <div className="search-box__container">
                 <input type="text" placeholder="Innova..." name="search" onChange={(e) => setTipe(e.target.value)}></input>
                 {/* <input type="text" placeholder="Innova..." name="search"></input> */}
                  <button className="cari-mobil" type="submit">Cari Mobil</button>
            </div>
        </div>
      </div>
      </form>
      <CarList car={car}/>
    </>
  )
}

export default Search