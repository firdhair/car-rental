import React from 'react';
//import '../hero/hero.scss'
//import './about.css';
//import car from '../images/car2.png'

const About = () => {
  return (
    <div className="hero">
      <div className="hero__txt">
          <h3>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h3>
          <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
        </div>
        <div className="hero__img">
          <img src="/images/car2.png"></img>
        </div>
    </div>
  )
}

export default About