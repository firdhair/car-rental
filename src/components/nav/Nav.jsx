import React from 'react';
import {Link} from "react-router-dom"


const Nav = () => {
  return (
    <div className="wrapper-nav">
      <nav>
        <div className="nav-rental">
          <Link to='/'>
            <h4>Rental</h4>
          </Link>
        </div>
        <div className="nav-list">
          <ul>
            <li>Our Services</li>
            <li>Why Us</li>
            <li>Testimonial</li>
            <li>FAQ</li>
             <Link to='/login'>
              <button>Login</button>
            </Link>
          </ul>
        </div>
    </nav>
    </div>
  )
}

export default Nav