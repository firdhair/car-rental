import React from 'react';
import {Routes, Route, Link} from "react-router-dom"

const Footer = () => {
  return (
       <footer>
          <div class="footer-container">
        <div class="brand">
             <ul>
                <li><a href="#">Jalan Suroyo No. 161 Mayangan Kota <br/> Probolonggo 672000</a></li>
                <li><a href="">binarcarrental@gmail.com</a></li>
                <li><a href="">081-233-334-808</a></li>
            </ul>
        </div>
        <div class="layanan-kami">
            <ul>
                <li class=""><a href="#">Our services</a></li>
                <li><a href="">Why Us</a></li>
                <li><a href="">Testimonial</a></li>
                <li><a href="">FAQ</a></li>
            </ul>
        </div>
        <div class="tentang-kami">
            <ul>
                <li class=""><a href="#">Connect with us</a></li>
            </ul>
             <ul class="footer-logo">
                <li>
                    <a href="https://www.instagram.com/ardawalika_eo/"><img src="/images/instagram.svg" alt=""/></a>
                </li>
                <li>
                    <a href=" https://www.facebook.com/Ardawalika-Event-Organizer-105289328788292"><img src="/images/facebook.svg" alt=""/></a>
                </li>
                <li>
                    <a href="https://wa.me/6282330304545"><img src="/images/whatsapp.svg" alt=""/></a>
                </li>
            </ul>
        </div>
        <div class="alamat">
            <ul>
                <li class=""><a href="#">Copyright Binar 2022</a></li>
            </ul>
        </div>
        </div>
       </footer>
  )
}

export default Footer